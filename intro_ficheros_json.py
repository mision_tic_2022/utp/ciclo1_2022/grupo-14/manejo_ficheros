import json 

persona = dict()
with open('persona.json') as archivo:
    persona: dict = json.load(archivo)
    print(persona['nombre'])
    print(persona['apellido'])

persona['genero'] = 'M'
print(persona)

#Sobreescribir el archivo json
with open('persona.json','w') as archivo:
    json.dump(persona, archivo)