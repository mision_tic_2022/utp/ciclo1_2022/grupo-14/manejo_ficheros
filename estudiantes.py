import json

'''
-----Registrar estudiante-----
*Nombre
*Apellido
*Edad
*Notas -> n notas
--------
{
    '1234567':{

    }
}
------
- Registrar estudiante
- Actualizar estudiante
- Registrar nota
- Actualizar nota 
- Eliminar nota
- Eliminar estudiante
- Eliminar todos los estudiantes
'''

def almacenar_datos(estudiantes: dict):
    with open('estudiantes.json', 'w') as archivo:
        json.dump(estudiantes, archivo)

def cargar_datos():
    estudiantes: dict = dict()
    try:
        with open('estudiantes.json') as archivo:
            estudiantes = json.load(archivo)
    except:
        estudiantes = dict()
    return estudiantes

def registrar_estudiante(estudiantes: dict):
    print('-----------------REGISTRAR ESTUDIANTE-------------')
    existe,cedula = validar_existencia(estudiantes)
    if existe:
        print(f"\nEl estudiante con cédula {cedula} ya se encuentra registrado\n")
    else:
        #Añadir info al diccionario estudiantes
        estudiantes[cedula] = {
        'nombre': input('Nombre: '),
        'apellido': input('Apellido: '),
        'edad': int(input('Edad: ')),
        'notas': []
        }
        almacenar_datos(estudiantes)
        print("\nEstudiante registrado con éxito\n")

def validar_existencia(estudiantes: dict):
    existe = False
    #Solicitar cédula
    cedula = input('Ingrese la cédula del estudiante: ')
    if cedula in estudiantes:
        existe = True
    return (existe, cedula)

def actualizar_estudiante(estudiantes: dict):
    print('---------------ACTUALIZAR ESTUDIANTE---------------')
    existe,cedula = validar_existencia(estudiantes)
    if existe:
        #Añadir info al diccionario estudiantes
        estudiantes[cedula] = {
        'nombre': input('Nombre: '),
        'apellido': input('Apellido: '),
        'edad': int(input('Edad: ')),
        'notas': estudiantes[cedula]['notas']
        }
        almacenar_datos(estudiantes)
        print('\nEstudiante actualizado con éxito\n')
    else:
        print('El estudiante no existe en la base de datos')

def registrar_notas(estudiantes: dict):
    existe,cedula = validar_existencia(estudiantes)
    if existe:
        cont = 1
        notas = []
        continuar = 'S'
        while continuar.lower() == 's':
            n = float( input(f'Ingrese nota {cont} >> ') )
            notas.append(n)
            cont += 1
            continuar = input('¿Desea registrar mas notas? S/N >> ')
        #Actualizar las notas en el diccionario  del estudiante
        estudiantes[cedula]['notas'] = estudiantes[cedula]['notas'] + notas
        almacenar_datos(estudiantes)
    else:
        print('\nEl estudiante no existe en la base de datos\n')

def mostrar_notas_x_estudiante(estudiantes: dict):
    existe,cedula = validar_existencia(estudiantes)
    if existe:
        info = estudiantes[cedula]
        nombre = info['nombre']
        apellido = info['apellido']
        print(f'-----{nombre} {apellido}-----')
        for n in info['notas']:
            print(f'\t-> {n}')
    else:
        print('\nEl estudiante no existe en la base de datos\n')

def mostrar_estudiantes(estudiantes: dict):
    #Iterar diccionario estudiantes
    for e in estudiantes.values():
        print('----------------------------------')
        nombre = e['nombre']
        apellido = e['apellido']
        edad = e['edad']
        print(f'Nombre: {nombre}\nApellido: {apellido}\nEdad: {edad}\nNotas: ')
        #Iterar notas
        for n in e['notas']:
            print(f'\t-> {n}')
        print('----------------------------------')

def eliminar_estudiante(estudiantes: dict):
    print('--------------ELIMINAR ESTUDIANTE-------------')
    existe,cedula = validar_existencia(estudiantes)
    if existe:
        #Eliminar estudiante del diccionario
        estudiantes.pop(cedula)
        #Sobreescribir el fichero json
        almacenar_datos(estudiantes)
        print('\nEstudiante eliminado con éxito\n')
    else:
        print('\nEl estudiante no existe en la base de datos\n')

def eliminar_todo(estudiantes: dict):
    print('--------------ELIMINAR TODOS LOS DATOS-------------')
    eliminar = input('¿Está seguro de eliminar todos los datos? S/N >> ')
    if eliminar.lower() == 's':
        estudiantes.clear()
        almacenar_datos(estudiantes)
        print("Datos eliminados con éxito")
        print("La base de datos se encuentra vacía")

def menu():
    #Diccionario que representa a todos los estudiantes
    estudiantes = cargar_datos()
    #
    mensaje = '--------------------CRUD ESTUDIANTES-------------\n'
    mensaje += '1) Registrar estudiante\n'
    mensaje += '2) Actualizar estudiante\n'
    mensaje += '3) Registrar nota\n'
    mensaje += '4) Mostrar estudiantes\n'
    mensaje += '5) Mostrar notas por estudiante\n'
    mensaje += '6) Eliminar estudiante\n'
    mensaje += '7) Eliminar todos los estudiantes\n'
    mensaje += '8) Salir\n'
    mensaje += '>>> '
    #Variable que representa la opción ingresada por el usuario
    opc = ''
    while opc != 8:
        opc = int( input(mensaje) )
        #Evaluar la opción del usuario
        if opc == 1:
            registrar_estudiante(estudiantes)
        elif opc == 2:
            actualizar_estudiante(estudiantes)
        elif opc == 3:
            registrar_notas(estudiantes)
        elif opc == 4:
            mostrar_estudiantes(estudiantes)
        elif opc == 5:
            mostrar_notas_x_estudiante(estudiantes)
        elif opc == 6:
            eliminar_estudiante(estudiantes)
        elif opc == 7:
            eliminar_todo(estudiantes)
        elif opc == 8:
            print("Ingrese una opción válida")
            

menu()